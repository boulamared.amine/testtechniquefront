import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/User';

@Injectable()
export class UserService{

    apiUrl: string = "http://localhost:8080/api";

    constructor(private http:HttpClient){

    }


    getAllUsers():Observable<any>{
        return this.http.get(this.apiUrl+"/users");
    }


    getAllProfiles():Observable<any>{
        return this.http.get(this.apiUrl+"/profiles");
    }


    createUser(user:User): Observable<any>{
        return this.http.post(this.apiUrl+"/users",user);
    }

    createUserWithImage(formData:FormData,user:User){
        user.photoPath = "https://theimag.org/wp-content/uploads/2015/01/user-icon-png-person-user-profile-icon-20-300x300.png";
        this.createUser(user).subscribe((data)=>console.log(data));
        return this.http.post(this.apiUrl+"/create",formData);
    }
}