export class User{

    id: number;
    nom: string;
    prenom: string;
    email: string;
    photoPath: string;
    password: string;
    code: string;
    profile: string;
    exipirationDate : Date;
    isEmail : boolean;
    isExpiring: boolean;
    isGenerated: boolean;
    constructor(){
        
    }
}