import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/userService';
import { User } from 'src/app/shared/models/User';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {

  users: [];
  myUsers : User[];
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getAllUsers().subscribe(
      (data)=> {
        this.users= data;
        this.users.map((user) => this.userDTOtoUser(user) );
        this.myUsers = this.users;
      },

    );
  }


  userDTOtoUser(data){
    let user = new User();
    user =Object.assign({},data);
  }
}
